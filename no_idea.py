#https://www.hackerrank.com/challenges/no-idea/problem

def no_idea(array, A, B):
    array_set = set(array)
    common_with_A = array_set.intersection(A)
    common_with_B = array_set.intersection(B)
    happiness = 0

    for element in array:
        if element in common_with_A:
            happiness += 1
        elif element in common_with_B:
            happiness -= 1
    return happiness

if __name__=='__main__':
    no_of_lines = 4
    content = []
    for i in range(no_of_lines):
        line = input()
        content.append(line)

    array = [int(e) for e in content[1].split(' ')]
    A = [int(e) for e in content[2].split(' ')]
    B = [int(e) for e in content[3].split(' ')]
    print(no_idea(array, A, B))

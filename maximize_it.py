#https://www.hackerrank.com/challenges/maximize-it/problem

from itertools import combinations, product, chain

def f(x):
    return x**2

def s(M, *argv):
    for arg in argv:
        return sum(f(arg))%M


#input multiline data
def multi_line_inp():
    while True:
        inp = input()
        if not inp: break
        yield inp


input_lines = list(multi_line_inp())

K = int(input_lines[0].split()[0])
M = int(input_lines[0].split()[1])
N = []

for line in input_lines[1::]:
    l = line.split()
    it = [int(i) for i in  l[1::]]
    N.append(it)

pairs = product(*N)

print(list(pairs))
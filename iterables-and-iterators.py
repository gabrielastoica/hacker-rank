#https://www.hackerrank.com/challenges/iterables-and-iterators/problem

from itertools import combinations

N = int(input())
l = input().split()
k = int(input())

target_character = l[k-1]
target_indexes = []

for i in range(len(l)):
    if l[i] == target_character:
        target_indexes.append(i+1)

c = list(combinations(range(1,N+1),k))
lc = len(c)
no_of_t = 0


for t in c:
    for i in t:
        if i in target_indexes:
            no_of_t += 1
            break
print(no_of_t/lc)

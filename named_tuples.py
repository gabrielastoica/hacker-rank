from collections import namedtuple

def multiline_input():
    while True:
        try:
            inp = input()
        except:
            return
        if not inp:
            break
        yield inp

input_lines = list(multiline_input())
n = int(input_lines[0])
columns = input_lines[1].split()
marks = columns.index('MARKS')
Student = namedtuple('Student', columns)
S = []

for _ in input_lines[2::]:
    line = _.split()
    line[marks] = int(line[marks])
    S.append(Student(*line))

sum_of_marks = 0
for s in S:
    sum_of_marks += s.MARKS

print("{0:.2f}".format(sum_of_marks/len(S),))


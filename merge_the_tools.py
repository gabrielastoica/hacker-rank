#https://www.hackerrank.com/challenges/merge-the-tools/

def merge_the_tools(string, k):
    substrings = []
    for i in range(int(len(string)/k)):
        substrings.append(string[i*k:(i+1)*k])

    for s in substrings:
        print("".join(sorted(set(s), key=s.index)))

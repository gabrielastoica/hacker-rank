from collections import deque

def multiline_input():
    while True:
        try:
            inp = input()
        except:
            return
        if not inp:
            break
        yield inp


data = list(multiline_input())
no_of_testcases = int(data[0])
tests = {}

for n in range(no_of_testcases):
    no_of_cubes = int(data[2*n + 1])
    tests[no_of_cubes] = data[2*n+2].split()

for k, v in tests.items():
    d = deque(v)
    result = "Yes"
    while d:
        m = max(d)
        if d[0] != m and d[-1] != m:
            result = "No"
            break
        elif d[0] == m:
            d.popleft()
        else:
            d.pop()
    print(result)

#https://www.hackerrank.com/challenges/find-angle/problem

from math import asin, sqrt, degrees
degree_sign= u'\N{DEGREE SIGN}'
AB = int(input())
BC = int(input())
s = ((AB/2) / sqrt(AB**2/4 + BC**2/4))
t = degrees(asin(s))
print(str(round(t))+degree_sign)

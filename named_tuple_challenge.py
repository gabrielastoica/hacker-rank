# https://www.hackerrank.com/challenges/py-collections-namedtuple/problem

from collections import namedtuple

data = []
N = int(input())
for i in range(0, N+1):
    line = input().split()
    data.append(line)

attributes = ' '.join(data[0])

student = namedtuple('student',attributes)
students = []

for i in range(1, N):
    students.append(student(*data[i]))

s = 0
for student in students:
    s += int(student.MARKS)

print("{0:.2f}".format(s/N))
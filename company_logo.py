from collections import Counter
import operator

name = list(input())
c = Counter(name)
sorted_by_keys = dict(sorted(c.items(), key=operator.itemgetter(0)))
sorted_by_values = dict(sorted(sorted_by_keys.items(), key=operator.itemgetter(1), reverse=True))

i = 0

for k, v in sorted_by_values.items():
    if i<3:
        i += 1
        print(k, v)

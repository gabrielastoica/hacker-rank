from collections import Counter

def multiline_input():
    while True:
        try:
            inp = input()
        except:
            return
        if not inp:
            break
        yield inp


data = list(multiline_input())
n = int(data[0])
c = Counter(data[1::])
print(len(c.keys()))
print(*list(c.values()))
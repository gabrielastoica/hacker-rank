from collections import OrderedDict
import re
d = OrderedDict()

N = int(input())

for _ in range(N):
    product, space, price = input().rpartition(" ")
    d[product] = d.get(product,0) + int(price)
print(*(' '.join([product, str(price)]) for product, price in d.items()), sep='\n')


